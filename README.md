# bash_history
DX OS component

# .bashrc - settings

## lines

Memory Lines: HISTSIZE=5000

Disk Lines: HISTFILESIZE=10000

## append
shopt -s histappend

## append immeditely, clear, run
export PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"


# parse history
history | grep cd

# output to a dashboard or preeeety file - todo

or you can do it - let me know or fork here

Recommendation: outfile at the end of the session for information purposes

# Reasons to use bash history
While the reasons seem obvious - to capture shell usage during testing
* To understand and determine any errors
* To diagnose any errors
* To understand the interaction in the terminal in detail
* To improve the developer experience based on the HCI
